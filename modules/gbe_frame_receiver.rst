##################
gbe_frame_receiver 
##################

*******
Purpose
*******

This entity handles the RX data stream from the MAC, and is responsible for 
decoding basic informations (like destination MAC adddress, ether type, protocols).
Moreover, basic sanity checks (protocol specific) are implemented here.

**IMPORTANT:** a very basic "write only" access by specially crafted UDP frames is 
implemented here, as an emergency access to very basic functionalities of the core 
(like turning on/off DHCP, or setting fixed IP addresses).

*****************
How does it work?
*****************

The ``gbe_frame_receiver`` expects a standard MAC RX datastream. Every frame start is detected,
and the incoming data stream is parsed for destination and source MAC address and the ethernet type.
The MAC attached should be in promiscious mode.

Frames which are not fitting the criteria for further handling (i.e. wrong destination MAC address, 
wrong ether type) are skipped in the very beginning and not even stored.

The payload of frames which are accepted are stored in a large data ringbuffer. During storage, online
checks on frame sanity are being performed (like frame length and structure for TRBnet SlowControl).
Should a frame being stored fail hard on any of these sanity checks, the storage will not be finished and 
the ring buffer write address is restored.

If a frame is accepted, the decoded informations are stored in several auxiliary FIFOs (including the 
payload length), and the ``gbe_receive_control`` unit is informed about a new frame to be forwarded to
``gbe_main_control``.

**Important:** the "secret" write only access works only with frames being dropped here.

*********
Interface
*********

To be written.

********
Generics
********


BUF_SIZE
   Defines the buffer size used in RX ring buffer. 4kB is usually a good choice,
   going for larger sizes may have negative impact on PAR and timing closure.

***********
Limitations
***********

* VLANs are not supported anymore.
* Jumbo frames not fitting the FIFO are discarded.
* Fragmented packets are not supported, frames which are part of a fragmented packet are dropped.

**********
Known bugs
**********
