#############################
gbe_response_constructor_DHCP
#############################

*******
Purpose
*******

Handling of DHCP requests, i.e. acquiring an IP address from the DHCP server.

*****************
How does it work?
*****************

Please check `DHCP description <https://en.wikipedia.org/wiki/Dynamic_Host_Configuration_Protocol>`_ for DHCP basics.

This entity, if compiled in and activated, will start sending out DHCPDISCOVER messages, and wait for an DHCPOFFER
to arrive. As next step, a DHCPREQUEST will be issued and after a successful DHCPACK received, normal operation will be assumed.

After a network interruption or any other action affecting the link, a new cycle will be started.

**NOTE:** DHCP will most likely need ARP and PING functionalities to work in modern networks.

You can use the OOB registers to deactivate the DHCP client, and assign an IP address manually. If DHCP fails, the
board will only answer to OOB accesses and a Ping Of Death (by broadcast MAC address only).

*********
Interface
*********

To be written.

********
Generics
********

None.

***********
Limitations
***********

None known.

**********
Known bugs
**********

None known.
