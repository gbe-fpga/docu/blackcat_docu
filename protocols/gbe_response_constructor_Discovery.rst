##################################
gbe_response_constructor_Discovery
##################################

*******
Purpose
*******

Simple "homebrewn" protocol to detect BlackCat based modules in the network

*****************
How does it work?
*****************

**WORK IN PROGRESS**

There is only one functionality implemented so far: if a Discovery frame is being received,
the board will answer with its current IP address and MAC address. Broadcast requests are supported.

**DO NOT USE THIS YET**

*********
Interface
*********

To be written.

********
Generics
********

None.

***********
Limitations
***********

None known.

**********
Known bugs
**********

None known.
