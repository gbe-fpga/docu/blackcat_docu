#################
gbe_logic_wrapper 
#################

*******
Purpose
*******

This module contains all function blocks for a GbE ethernet core.

*****************
How does it work?
*****************

There is one central steering entity, named ``gbe_main_control``, which synchronizes all RX and TX activities.

Received frames are forwarded to specific protocol handler, and frames to be transmitted are collected from the 
same protocol handlers.

Let's take a look at the RX path first:

Inside this entity, the data stream from the MAC is taken over by the ``gbe_frame_receiver``, which decodes
basic information from the frame and does some (also protocol specific) sanity checks. Only valid frames 
directed at the FPGA will be stored for further usage, all other frames will be dropped.

The ``gbe_receive_control`` is the next part, it acts mainly as state machine interface for retrieving the frames
stored inside the ``gbe_frame_receiver`` and forwarding it to the protocol handler in charge.

The TX path starts inside the protocol handlers (located inside ``gbe_protocol_selector`` entity). Frames are prepared
inside the handlers, and forwarded to the ``gbe_transmit_control``, where frames are fragmented if necessary. 

As next step, the ``gbe_frame_constr`` prepares frames for sending out and transfers them to the MAC.

Protocols are handled inside the ``gbe_protocol_selector``. For any protocol to be used (be it RX only, TX only, or RX/TX) 
a separate entity is needed. Each protocol handler is responsible for parsing the RX frame and constructing a suitable TX frame.

*********
Interface
*********

To be written.

********
Generics
********

.. _gbe_logic_wrapper_generics_label:

There are some generics influencing the behaviour of the core:

BUF_SIZE
   Defines the buffer size used in RX and TX pathes. 4kB is usually a good choice,
   going for larger sizes may have negative impact on PAR and timing closure.

SLOWCONTROL_BUFFER_SIZE
   Defines the TX buffer size for TRBnet slow control access.

LINK_HAS_PING
   Includes a simple ICMP ping handler. Useful for normal operation, and (sometimes) mandatory
   for DHCP, as some hosts check the presence of an IP by a ping request.

LINK_HAS_ARP
   Mandatory normal ethernet operation, to allow reverse lookup of IPs and MACs.

LINK_HAS_DHCP
   Includes a simple DHCP client. Be sure to also include ARP and PING if you want DHCP.

LINK_HAS_SCTRL
   Includes a simple TRBnet slow control endpoint. The endpoint lacks an internal hub, and is limited
   to slow access (usually half of the link speed). Access can be made by standard ``trbnetd`` and ``trbcmd`` et. al.

LINK_HAS_FWD
   Includes a simple ethernet transmission entity. **UNTESTED**

LINK_HAS_DISCOVERY
   Includes a simple (new) protocol to identify all modules operating BlackCat in the network. **WORK IN PROGRESS**


***********
Limitations
***********

One RX queue only, so if one protocol handler is stuck, the whole core will be stuck.

**********
Known bugs
**********

None known, but for sure some still are lurking around.
