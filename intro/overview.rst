#####################
BlackCat GbE Overview
#####################

The BlackCat project provides a lightweight, portable and (hopefully) versatile GbE ethernet core
mainly for Lattice ECP3/ECP5 FPGAs. The code is in use also on Nexus FPGAs now, and has been used 
successfully on Xilinx chips.

This code is based on Greg's efforts to create a GbE port for TRBnet and has been used in the DeepSea 
TRBnet branch, which at the same time is depreciated.

As slowcontrol protocol DOGma has been chosen. It replaces the TRBnet approach used in the very first 
version of BlackCat.

In addition, syntonous operation of GbE is supported, as well as Link Delay measurements. 
A macro TDC can be included into ECP3 nodes, and will handle all timing measurements.

**********
Philosophy
**********

BlackCat is aiming at experiments where timing and data transport have to be done over one fiber only,
with a minimum of wiring between the modules in the system.

BlackCat is targeted at the P-ONE experiment, but can be used for other purposes.

The base idea of BlackCat is to treat all modules in the system in the same way: all modules can be used
as FiFo (Fan In Fan Out) for GbE connections. A small "hub" can simply be included, and allows to connect more 
modules, or provide "normal" GbE ports for local CPUs.

In terms of link delay measurements, ports can individually be setup for this special type of ethernet, or 
fall back to standard IEEE ethernet for external CPUs.

**********
Code parts
**********

BlackCat is divided into several parts:

gbe_core
   technology independent parts of GbE - this code is not fixed to any vendor specific stuff, and should
   compile for any FPGA family.
   
gbe_ecp3
   includes all vendor specific cores and IP cores for Lattice ECP3 FPGA family.

gbe_ecp5
   includes all vendor specific cores and IP cores for Lattice ECP5 FPGA family.

gbe_ecp2m
   includes all vendor specific cores and IP cores for Lattice ECP2M FPGA family (EXPERIMENTAL).

trb3sc_*
   module specific code for TRB3sc1 modules

trb5sc_*
   module specific code for TRB5sc1 modules

trb5sc2_*
   module specific code for TRB5sc2 modules

tomcat*
   module specific code for TOMcat modules
   
