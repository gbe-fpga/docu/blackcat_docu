###################
gbe_receive_control 
###################

*******
Purpose
*******

This entity is kind of legacy, it only contains a small state machine to
transfer one frame from the ``gbe_frame_receiver`` to the ``gbe_main_control`` unit.

*****************
How does it work?
*****************

Basically, just a forwarder in here. It waits for a complete frame stored in the
``gbe_frame_receiver`` and handles the transfer.

This entity is left "as is" to keep symmetry to the TX path.

*********
Interface
*********

To be written.

********
Generics
********

None.

***********
Limitations
***********

None known.

**********
Known bugs
**********

None known.
