############################
gbe_response_constructor_ARP
############################

*******
Purpose
*******

Handling of ARP requests, i.e. allowing the resolution of IP address and MAC address.

*****************
How does it work?
*****************

Please check `ARP description <https://en.wikipedia.org/wiki/Address_Resolution_Protocol>`_ for ARP basics.

Incoming ARP requests will trigger a reply according to ARP standard. 

*********
Interface
*********

To be written.

********
Generics
********

None.

***********
Limitations
***********

None known.

**********
Known bugs
**********

None known.
