################
gbe_wrapper_fifo 
################

*******
Purpose
*******

This is the most basic wrapper for the whole GbE core. It includes as only part
the ``gbe_logic_wrapper`` and is used to adopt the RX and TX stream interfaces to your need.

In BlackCat case, the FIFO interface is used, which allows simple DMA'ing of ethernet frames.

*****************
How does it work?
*****************

It's a wrapper, so no real functionality is included here.

*********
Interface
*********

To be written.

********
Generics
********

There are some generics influencing the behaviour of the core.

Please see :ref:`gbe_logic_wrapper description <gbe_logic_wrapper_generics_label>` for details.

***********
Limitations
***********

None known.

**********
Known bugs
**********

None known.
