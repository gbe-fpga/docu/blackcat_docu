#####################
gbe_protocol_selector 
#####################

*******
Purpose
*******

This entity distributes the RX frames stored in the ``gbe_frame_receiver`` to specific handlers.
In addition, handlers request access to transmit frames (being it replies to requests or stand-alone TX actions).

*****************
How does it work?
*****************

A simple state machine handles the multiplexing and demultiplexing of data streams. No magic in here.

*********
Interface
*********

To be written.

********
Generics
********

There are some generics influencing the behaviour of the core.

Please see :ref:`gbe_logic_wrapper description <gbe_logic_wrapper_generics_label>` for details.


***********
Limitations
***********

A stuck protocol handler will also stuck the protocol selector logic.

**********
Known bugs
**********

None known.
