################
gbe_main_control 
################

*******
Purpose
*******

This entity handles all data steering actions inside the core. It distributes incoming frames 
to the protocol handlers and manages the multiplexing of outgoing frames.

Moreover, the link status is monitored here and distributed to all entities.

*****************
How does it work?
*****************

The RX data is forwarded to the ``gbe_protocol_selector`` and distributed to the protocol handler in 
charge for the specific frame. Independent of RX operation the entity takes care to forward 
frames from the ``gbe_transmit_control`` unit to the MAC.

In addition the link status is monitored here, and in case of DHCP being active, the link is only activated 
after an IP has been acquired by DHCP.

*********
Interface
*********

To be written.

********
Generics
********

There are some generics influencing the behaviour of the core.

Please see :ref:`gbe_logic_wrapper description <gbe_logic_wrapper_generics_label>` for details.

***********
Limitations
***********

None known.

**********
Known bugs
**********

None known.
