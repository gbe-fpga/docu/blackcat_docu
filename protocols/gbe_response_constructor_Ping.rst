#############################
gbe_response_constructor_Ping
#############################

*******
Purpose
*******

Simple ICMP ping request handler. 

*****************
How does it work?
*****************

Please check `ICMP description <https://en.wikipedia.org/wiki/Internet_Control_Message_Protocol>`_ for ICMP basics.

Incoming ICMP requests will trigger a reply according to ICMP standard. 

This handler simply echoes the received payload back to the sender. Only ping requests are supported,
all other requests will be silently dropped.

*********
Interface
*********

To be written.

********
Generics
********

None.

***********
Limitations
***********

* Maximum payload is 2040 bytes, oversized payload frames are dropped.
* Fragmentation of reply is not supported, so make sure your MTU and the payload size match.

**********
Known bugs
**********

None known.
