###############################
gbe_response_constructor_SCTRL2
###############################

*******
Purpose
*******

A simple and function reduced TRBnet SlowControl endpoint.

*****************
How does it work?
*****************

This handler implements a simple TRBnet SlowControl endpoint, allowing to use allowing
TRBnet software for slow control purposes without any changes.

In this handler, a strict clock domain transfer is implemented, so the slow control bus 
can be operated at lower clock speeds to faciliate timing closure.

Do not mess around with the timing of the signals! It may have interference with 
the TRBnet units on the bus.

*********
Interface
*********

To be written.

********
Generics
********

SLOWCONTROL_BUFFER_SIZE
   Defines the TX buffer size for TRBnet slow control access.

***********
Limitations
***********

None known.

**********
Known bugs
**********

None known.
