.. BlackCat_Docu documentation master file, created by
   sphinx-quickstart on Wed Dec  7 14:17:52 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

GbE BlackCat documentation
=========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   intro/overview
   hardware/copper_sfp
   modules/gbe_wrapper_fifo
   modules/gbe_logic_wrapper
   modules/gbe_frame_receiver
   modules/gbe_receive_control
   modules/gbe_pod
   modules/gbe_main_control
   modules/gbe_protocol_selector
   protocols/gbe_response_constructor_ARP
   protocols/gbe_response_constructor_DHCP
   protocols/gbe_response_constructor_Discovery
   protocols/gbe_response_constructor_Forward
   protocols/gbe_response_constructor_Ping
   protocols/gbe_response_constructor_SCTRL2
   
   

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
