###############################
Using copper SFPs with BlackCat
###############################

The BlackCat protocol is to be used with fiber SFPs, but can also
be used with copper SFPs, as long as some basic rules are obeyed.

*****************
Copper SFP basics
*****************

Copper SFPs usually are active parts, implementing a small two port 
ethernet switch inside the SFP module. These modules always operate on
an internal 125MHZ clock, and **can not** be used to proliferate a central
clock or send kommas with defined timing.

Fiber SFPs - in contrast - are basically "passive" components without an
internal clock.

Copper SFPs alway introduce additional levels of ethernet autonegotiation,
and special rules apply for successful autonegotiation.

**********************
Autonegotiation issues
**********************

Copper SFPs have two ports, one "internal" one talking SerDes on the SFP module 
connector, one "external" one with RJ45 copper signals.

Both ports will do autonegotiation when connected, but with sequencing: a copper
SFP without copper cable attached will start autonegotiation to the FPGA SerDes, but will
not complete the process by sending and empty link capability advertisment page.

By connecting the external port to a link partner, autonegotiation on the external port
will take place and build up a link. After successful autonegotiation on the external port,
autonegotiation on the internal port will succeed.

************
Clock issues
************

Clock handling is an issue with copper SFPs, so we have to differentiate between slave ports
(uplink) and master ports (downlink).

Uplink usage
============

When a copper SFP is used on the uplink (slave port), the internal 125MHz clock of the SFP will
be used by the FPGA SerDes as recovered RX clock, and the SerDes TX channels will be taken into
operation. Ethernet autonegotiation will be stuck until a link partner is connected on the RJ45
connector. The FPGA will be operational without a link partner being present, but the clock on
RX and TX parts of the copper SFP will be the same, and the syntonous logic will be fine.

Downlink usage
==============

When a copper SFP is used on the downlink (master port), the TX part of FPGA SerDes will operate 
on the recovered RX clock of the uplink port. The RX part of FPGA SerDes will operate on the
internal 125MHz clock of the copper SFP, and this will create some problems.

Both clocks will usually differ by some tenth of PPMs, so the word syncing mechanism of the FPGA
RX channel will deliver a sync signal which usually will "run away" compared to the TX part.
The link may or may not start up, depending on the clock difference, and then continue operation 
(as CTC is implemented in the SGMII core).

For copper usage on downlinks, a control bit has to be set to disable the word sync proliferation.
