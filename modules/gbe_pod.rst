#######
gbe_pod
#######

*******
Purpose
*******

This entity takes care of the famous "Ping Of Death".

*****************
How does it work?
*****************

The ``gbe_pod`` entity must be connected directly to the RX MAC data stream. 
It **MUST NOT** be placed behind any multiplexer to keep it reachable even in case
of severe lockups.

A simple parser is decoding every incoming frame. Any IPv4 ICMP ping request, which is either
directed to the FPGA MAC address or sent as broadcast will be parsed.

The ping request payload is parsed for a magic number (*0xabad1dea* as first 32bit word of the payload).
If found the next 32bit word is parsed for the first byte only (**note:** ping payloads are always 32bit 
aligned!), which will define the action taken:

* *0xc3* will reset the FPGA (not implemented yet)
* *0xa5* will reload the FPGA from FlashROM

A correct ping of death will look like this for rebooting the FPGA ::

   ping 10.1.1.102 -p abad1deaa5000000 -c 1

A reset can be made by ::

   ping 10.1.1.102 -p abad1deac3000000 -c 1

If supported by your operating system, also a broadcast ping can be done: ::
   
   ping -b 10.1.1.255 -p abad1deaa5000000 -c 1

Any other command will be silently ignored.

**Important:** to reload a board which has not acquired a valid IP by DHCP (or has no ARP functionality compiled in)
the Ping Of Death will work only with a broadcast ping, affecting all modules with BlackCat code in your network.

**Keep in mind:** payload is given as hexadecimal string, without trailing "0x".

*********
Interface
*********

To be written.

********
Generics
********

None.

***********
Limitations
***********

None known.

**********
Known bugs
**********

None known.
